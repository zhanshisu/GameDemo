package cn.sfatec.gamedemo.bean;

/**
 * Created by James on 2016/8/22.
 * Note:武器实体类
 */
public class WeaponBean {
    /**
     * 武器名称
     */
    private String name;
    /**
     * 攻击力 默认为100滴血 根据汉子的不同等级，打不同等级的怪兽，
     */
    private int heat=100;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHeat() {
        return heat;
    }

    public void setHeat(int heat) {
        this.heat = heat;
    }
}

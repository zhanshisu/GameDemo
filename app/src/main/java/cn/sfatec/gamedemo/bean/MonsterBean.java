package cn.sfatec.gamedemo.bean;

/**
 * Created by James on 2016/8/22.
 * Note:怪兽实体类
 */
public class MonsterBean {
    /**
     * 怪兽角色
     */
    private String name;
    /**
     * 等级
     */
    private int step;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }
}

package cn.sfatec.gamedemo.impls;

import cn.sfatec.gamedemo.bean.ManBean;
import cn.sfatec.gamedemo.bean.MonsterBean;
import cn.sfatec.gamedemo.bean.WeaponBean;
import cn.sfatec.gamedemo.inface.FightInteface;

/**
 * Created by James on 2016/8/22.
 * Note:战斗接口的实现
 */
public class FightImpl implements FightInteface {

    @Override
    public int ManHeatMonster(ManBean manBean, MonsterBean monsterBean, WeaponBean weaponBean) {
        if(manBean!=null && monsterBean!=null && weaponBean!=null){
            return (weaponBean.getHeat()*manBean.getStep())/monsterBean.getStep();
        }
        return 0;
    }
}

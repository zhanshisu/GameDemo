package cn.sfatec.gamedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import cn.sfatec.gamedemo.bean.ManBean;
import cn.sfatec.gamedemo.bean.MonsterBean;
import cn.sfatec.gamedemo.bean.WeaponBean;
import cn.sfatec.gamedemo.impls.FightImpl;
import cn.sfatec.gamedemo.inface.FightInteface;

public class MainActivity extends AppCompatActivity {
    /**
     * 阿三选项
     */
    private RadioButton man1;
    /**
     * 三角龙选项
     */
    private RadioButton monster1;
    /**
     * 尚方宝剑选项
     */

    private RadioButton weapon1;
    /**
     * 开始攻击按钮
     */
    private TextView btnFight;
    /**
     * 结果
     */
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initData();

    }

    /**
     * 初始化数据
     */
    private FightImpl fight;
    private  ManBean manBean1;
    private ManBean manBean2;
    private MonsterBean monsterBean1;
    private MonsterBean monsterBean2;
    private WeaponBean weaponBean1;
    private WeaponBean weaponBean2;
    private void initData() {
        fight=new FightImpl();

        manBean1=new ManBean();
        manBean1.setName("阿三");
        manBean1.setStep(2);
        manBean2=new ManBean();
        manBean2.setName("宝宝");
        manBean2.setStep(20);

        monsterBean1=new MonsterBean();
        monsterBean1.setName("三角龙");
        monsterBean1.setStep(2);
        monsterBean2=new MonsterBean();
        monsterBean2.setName("飞龙");
        monsterBean2.setStep(10);

        weaponBean1=new WeaponBean();
        weaponBean1.setName("尚方宝剑");
        weaponBean2=new WeaponBean();
        weaponBean2.setName("屠龙刀");
        weaponBean2.setHeat(200);
    }

    /**
     * 初始化布局
     */
    private void initView() {
        man1= (RadioButton) findViewById(R.id.man1);
        monster1= (RadioButton) findViewById(R.id.monster1);
        weapon1= (RadioButton) findViewById(R.id.weapon1);
        btnFight= (TextView) findViewById(R.id.btnFight);
        result= (TextView) findViewById(R.id.result);
        btnFight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ManBean manBean=null;
                MonsterBean monsterBean=null;
                WeaponBean weaponBean=null;
                if(man1.isChecked()){
                    manBean=manBean1;
                }else{
                    manBean=manBean2;
                }
                if(monster1.isChecked()){
                    monsterBean=monsterBean1;
                }else{
                    monsterBean=monsterBean2;
                }
                if(weapon1.isChecked()){
                    weaponBean=weaponBean1;
                }else{
                    weaponBean=weaponBean2;
                }
                result.setText(manBean.getName()+"用"+weaponBean.getName()+"打"+monsterBean.getName()+"掉了"+fight.ManHeatMonster(manBean,monsterBean,weaponBean)+"滴血");
            }
        });
    }
}

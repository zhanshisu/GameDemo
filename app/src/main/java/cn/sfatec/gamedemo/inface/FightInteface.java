package cn.sfatec.gamedemo.inface;

import cn.sfatec.gamedemo.bean.ManBean;
import cn.sfatec.gamedemo.bean.MonsterBean;
import cn.sfatec.gamedemo.bean.WeaponBean;

/**
 * Created by James on 2016/8/22.
 * Note:战斗接口
 */
public interface FightInteface {

    public int ManHeatMonster(ManBean manBean, MonsterBean monsterBean, WeaponBean weaponBean);
}
